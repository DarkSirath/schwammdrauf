/** @type {import('tailwindcss').Config} */
const plugin = require('tailwindcss/plugin');

module.exports = {
  content: [
    "./src/**/*.{tsx,css}",
  ],
  theme: {
    extend: {
      colors: {
        "primary-color": "#fafafa",
        "secondary-color": "#a99985",
        "signal-color": "#226524",
        "primary-text-color": "#000000",
        "link-color": "#005262",
      },
      fontFamily: {
        big_shoulders: ["BigShouldersStencilDisplay","sans-serif"],
      },
    },
  },
  plugins: [
    plugin(({ addBase, theme }) => {
      addBase({
        '.scrollbar': {
            overflowY: 'auto',
            scrollbarColor: `${theme('colors.signal-color')} ${theme('colors.primary-color')}`,
            scrollbarWidth: 'thin',
        },
        '.scrollbar::-webkit-scrollbar': {
            height: '2px',
            width: '2px',
        },
        '.scrollbar::-webkit-scrollbar-thumb': {
            backgroundColor: theme('colors.primary-color'),
        },
        '.scrollbar::-webkit-scrollbar-track-piece': {
            backgroundColor: theme('colors.primary-color'),
        },
      });
    }),
    require('@tailwindcss/typography'),
  ],
}

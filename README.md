# Schwamm Drauf - DataStory

## Lokales Testen (Developing)
### Initiales Setup
- Führe ```npm install``` aus, um die Bibliotheken zu installieren.
### Starte das Projekt
- Starte einen lokalen Test Server via: ```npm run dev```
- Öffne [http://localhost:3000](http://localhost:3000) mit deinem Browser, um das Ergebnis zu sehen

## Nutzen auf Server (Production)
[Tbd.]

## Aufbau
Das ist ein [Next.js](https://nextjs.org/) Projekt, welches hauptsächlich die folgenden Bibliotheken nutzt:
- Tailwind: Bietet CSS-freies HTML durch Nutzung spezifischer Tailwind Klassen
- D3.js: Zur darstellung von Diagrammen
- Typescript: Typisierung von Javascript
- OpenLayers: Einbindung der 2D Karte

![Mindmap der technischen Umsetzung](/Mindmap_Technische_Umsetzung.svg)

## Tipps & Tricks
- Für die Anzeige von Hints und Code-Komplementierung bei Tailwind, kannst du das Plugin "Tailwind CSS IntelliSense" in VSCode installieren


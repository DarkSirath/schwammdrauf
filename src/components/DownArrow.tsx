import { scrollToNextElement } from '@/services/scrollHelper'
import Image from 'next/image'

export default function DownArrow({duration, next_id}:any) {
    return (
        <div className="flex flex-row h-[50px] justify-center
                        max-2xl:h-[30px] my-2">
            <button className="relative rounded-full h-full aspect-square"
            onClick={() => scrollToNextElement(next_id, duration)}>
                <Image src="./arrow_down.svg" fill
                alt="Pfeil nach unten"
                sizes="50px"
                style={{objectFit: "contain"}}></Image>
            </button>
        </div>
    )
}
import Link from "next/link";

export default function StandardLink({ children, href, id }:any) {
    return (
        <Link id={id} className="text-link-color hover:text-primary-text-color" href={href}>
            {children}
        </Link>
    )
}
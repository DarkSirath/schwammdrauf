import { scrollToNextElement } from '@/services/scrollHelper'
import Image from 'next/image'

export default function UpArrow({duration, prev_id}:any) {
    return (
        <div className="flex flex-row h-[50px] justify-center
                        max-2xl:h-[30px] my-2">
            <button className="relative rounded-full h-full aspect-square"
            onClick={() => scrollToNextElement(prev_id, duration)}>
                <Image src="./arrow_up.svg" fill
                alt="Pfeil nach oben"
                sizes="50px"
                style={{objectFit: "contain"}}></Image>
            </button>
        </div>
    )
}
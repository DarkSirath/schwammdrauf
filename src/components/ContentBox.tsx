import { scrollToNextElement } from '@/services/scrollHelper'
import Image from 'next/image'
import DownArrow from './DownArrow'
import UpArrow from './UpArrow'

export default function ContentBox({ children, title, id, length, next_id, prev_id }:any) {
    return (
        <div className="flex flex-col w-full" style={{height:length+"vh"}}>
            <div id={id} className="flex flex-col w-full h-[100vh] my-[25vh] z-10 ml-auto justify-center sticky top-0
            max-lg:justify-end">
                <div className="flex flex-col w-full h-fit z-10 
                max-lg:h-[45vh] max-lg:mb-5 max-lg:z-0
                max-sm:h-[65vh]">
                    <div className="w-full h-fit mb-auto bg-white border rounded-md drop-shadow-lg 
                    max-sm:overflow-y-scroll max-sm:touch-pan-y max-sm:overscroll-y-auto">
                        <div className="p-5 h-fit">
                            <h2 className="text-xl 2xl:text-2xl mb-3 2xl:mb-7 uppercase font-bold">{title}</h2>
                            {children}
                        </div>
                    </div>
                    <div className="w-full h-fit flex justify-center space-x-5">
                        <DownArrow next_id={next_id} duration={length*10}/>
                        <UpArrow prev_id={prev_id} duration={length*10}/>
                    </div>
                </div> 
            </div>
        </div>
    )
}
import Image from 'next/image'

export default function LegendBox({ children, title, id }:any) {
    return (
        <div id={id} className="flex flex-col bg-white p-5 rounded-md drop-shadow-md z-20 m-5 border
        max-sm:h-full max-sm:m-0 max-sm:p-3 max-sm:drop-shadow-none" style={{display:"none"}}>
            <div>
                <div className="font-semibold mb-4
                max-sm:mb-2">{title}</div>
                <div className="flex flex-row-reverse">
                    <div className="w-[20px] bg-gradient-to-b from-[rgb(255,0,0)] via-[rgb(255,255,0)] to-[rgb(0,0,255)] rounded-md border border-black
                    max-sm:w-[15px]"></div>
                    <div className="flex flex-col mr-5 justify-between text-sm w-max">
                        {children}
                    </div>
                </div>
            </div>
        </div>
    )
}
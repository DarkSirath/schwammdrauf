export default function DefaultTooltip({ children, id }:any) {
    return (
        <div id={id} className="bg-white w-[300px] h-fit p-5 rounded-md hidden drop-shadow-lg">
            {children}
        </div>
    )
}
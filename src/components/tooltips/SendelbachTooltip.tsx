import DefaultTooltip from "./DefaultTooltip";
import Image from 'next/image'

export default function SendelbachTooltip({id}:any) {
    return (
        <DefaultTooltip id={id}>
            <h2 className="text-lg 2xl:text-xl mb-2">Klimastation Bamberg</h2>
            <p className="mb-2">
                Messungen u.a. zu Niederschlag, Temperatur, Luftfeuchtigkeit, Sonnenscheindauer und
                Windgeschwindigkeit
            </p>
            <div className="w-full aspect-[16/9] relative">
                <Image src="/images/Klimastation_DWD.JPG" fill
                alt="Foto der DWD Klimastation" 
                sizes="300px"
                style={{objectFit: "cover"}}></Image>
            </div>
            <p>Bild: Thomas Foken</p>
        </DefaultTooltip>
    )
}
import { Zone } from "@/classes/tooltips/Zone";
import DefaultTooltip from "./DefaultTooltip";

class ZoneTooltipClass{
    id!:string;
    zone!:Zone|undefined;
}

export default function ZoneTooltip({id, zone}:ZoneTooltipClass) {
    if(!zone){
        return <DefaultTooltip id={id}/>
    }
    return (
        <DefaultTooltip id={id}>
            <h2 className="text-lg 2xl:text-xl mb-2" >
                <span className="text-white rounded-md px-2" style={{backgroundColor: zone.color}}>LCZ <span/>{zone.lcz}</span> - {zone.name}
            </h2>
            <div>
                <div className="flex flex-row">
                    <p>{zone.description}</p>
                </div>
                {(zone.temperature)?
                <div className="flex flex-row">
                    <p className="w-1/2 font-semibold">Temperatur</p>
                    <p>{zone.temperature}  °C</p>
                </div>
                :<></>
                }
                {(zone.divergence)?
                <div className="flex flex-row">
                    <p className="w-1/2 font-semibold">Abweichung</p>
                    <p>{zone.divergence} °C</p>
                </div>
                :<></>
                }
            </div>
        </DefaultTooltip>
    )
}
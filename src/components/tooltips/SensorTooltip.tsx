import { Sensor } from "@/classes/tooltips/Sensor";
import DefaultTooltip from "./DefaultTooltip";

class SensorTooltipClass{
    id!:string;
    sensor!:Sensor|undefined;
}

export default function SensorTooltip({id, sensor}:SensorTooltipClass) {
    if(!sensor){
        return <DefaultTooltip id={id}/>
    }
    return (
        <DefaultTooltip id={id}>
            <h2 className="text-lg 2xl:text-xl mb-2">Netatmo-Sensor</h2>
            <div>
                <div className="flex flex-row">
                    <p className="w-1/3 font-semibold">Straße</p>
                    <p>{sensor.street}</p>
                </div>
                <div className="flex flex-row">
                    <p className="w-1/3 font-semibold">Höhe</p>
                    <p></p>{sensor.altitude}<p>m</p>
                </div>
                <div className="flex flex-row">
                    <p className="w-1/3 font-semibold">Läuft seit</p>
                    <p>{sensor.setup}</p>
                </div>
                <div className="flex flex-row">
                    <p className="w-1/3 font-semibold">Gehört zu</p>
                    <p id="sensor-belongto">{sensor.belongsTo}</p>
                </div>
            </div>
        </DefaultTooltip>
    )
}
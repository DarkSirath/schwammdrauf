import DefaultTooltip from "./DefaultTooltip";
import Image from 'next/image'

export default function StauffenbergTooltip({id}:any) {
    return (
        <DefaultTooltip id={id}>
            <h2 className="text-lg 2xl:text-xl mb-2">Wartehäuschen Stauffenberg-Schule</h2>
            <p className="mb-2">
                Dachbegrünung auf dem Wartehäuschen
            </p>
            <div className="w-full aspect-[16/9] relative">
                <Image fill
                src="/images/Haltestelle_Stauffenberg.jpg" 
                alt="Foto Haltestelle"
                sizes="300px"
                style={{objectFit: "cover"}}></Image>
            </div>
            <p>Bild: Florian Aschinger</p>
        </DefaultTooltip>
    )
}
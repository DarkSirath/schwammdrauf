import Link from 'next/link'
import Image from 'next/image'
import StandardLink from './StandardLink'

export default function Footer(){
    return (
        <>
        <footer id="footer" className="bg-white w-[90%] pt-[75px] pb-[75px] mx-auto 
        max-lg:w-[90%] text-[12pt]">
            <div className='flex flex-row w-[80%] justify-between mx-auto
            max-lg:w-[90%] max-lg:flex-wrap'>
                <div className="flex flex-col justify-start items-start w-1/3
                max-lg:w-1/2
                max-sm:w-full">
                    <div className="text-[16pt] font-semibold">
                        Quellen
                    </div>
                    <div className="mt-5">
                        <StandardLink id="DWD" href="https://www.dwd.de/DE/wetter/wetterundklima_vorort/bayern/bamberg/_node.html">
                            <p>[1] Wetterdaten Bamberg (DWD)</p>
                        </StandardLink>
                        <StandardLink id="NETATMO" href="https://weathermap.netatmo.com/">
                            <p>[2] Wetterkarte Netatmo</p>
                        </StandardLink>
                        <StandardLink id="LCZ" href="https://journals.ametsoc.org/view/journals/bams/93/12/bams-d-11-00019.1.xml">
                            <p>[3] Artikel "Local Climate Zones"</p>
                        </StandardLink>
                        <StandardLink id="IRS" href="https://bvm-bamberg.de/fileadmin/images/bvm/Insel-Rundschau/Inselrundschau-2023-Nr.35.pdf">
                            <p>[4] Inselrundschau 35</p>
                        </StandardLink>
                        <StandardLink id="GRW" href="https://www.stadtwerke-bamberg.de/gruene-wartehaeuschen-in-bamberg">
                            <p>[5] Artikel "Grüne Wartehäuschen"</p>
                        </StandardLink>
                        <StandardLink id="GDV" href="https://www.gdv.de/gdv/medien/medieninformationen/versiegelungsstudie-ludwigshafen-ist-die-am-staerksten-versiegelte-stadt-in-deutschland--133126">
                            <p>[6] GDV-Versiegelungsstudie</p>
                        </StandardLink>
                    </div>
                </div>
                <div className="flex flex-col justify-start items-start pr-[50px] w-1/3
                max-lg:w-1/2 max-lg:pr-0
                max-sm:w-full max-sm:mt-10">
                    <span className="text-[16pt] font-semibold">Schwamm Drauf</span> 
                    <p className="mt-5">Das Projekt ist im Rahmen eines Masterprojekts an der <StandardLink href="https://www.uni-bamberg.de/vis/">Universität Bamberg</StandardLink> entstanden.</p>
                    <p className="mt-5">Das Projekt ist vollständig Open Source und kann auf <StandardLink href="https://gitlab.com/DarkSirath/schwammdrauf">GitLab</StandardLink> gefunden werden. </p>
                    <p className="mt-5">Copyright 2023 Pascal Löffler (<StandardLink href="mailto:info@wolperlab.de">E-Mail</StandardLink>, <StandardLink href="https://www.linkedin.com/company/wolperlab/">LinkedIn</StandardLink>) und Florian Aschinger (<StandardLink href="mailto:florian.aschinger@posteo.de">E-Mail</StandardLink>)</p>
                </div>
                <div className="flex flex-col items-start w-1/3
                max-lg:w-1/2 max-lg:mt-10
                max-sm:w-full">
                    <div className="text-[16pt] font-semibold">
                        Betrieben und weiterentwickelt von
                    </div>
                    <Link href="https://www.wolperlab.de" className="w-[300px] h-[100px]">
                        <div className='relative w-full h-full'>
                            <Image src="/Logo_Wolperlab_Dark_Text.svg" alt="WolperLab Logo" fill style={{objectFit: "contain"}}></Image>
                        </div>
                    </Link>
                    <div className="text-[16pt] font-semibold">
                        Finanziert und unterstützt durch
                    </div>
                    <div className="w-full flex flex-col">
                        <Link href="https://bvm-bamberg.de" className="w-[300px] h-[100px]">
                            <div className='relative w-full h-full'>
                                <Image src="Logo_BVM.svg" alt="Bürgerverein Mitte Logo" fill style={{objectFit: "contain"}}></Image>
                            </div>
                        </Link>
                        <div className="w-full mt-5 ml-5">
                            <p>➦ Zum <StandardLink href="https://bvm-bamberg.de/projektseiten/klima/">Klimamessnetz</StandardLink></p>
                            <p>➦ Zu <StandardLink href="https://www.instagram.com/bv_bamberg_mitte/">Instagram</StandardLink></p>
                        </div>
                    </div>
                </div>
            </div>
        </footer> 
        </>
    )
}
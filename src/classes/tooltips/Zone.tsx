export class Zone{
    name!: string;
    lcz!: string;
    description!: string;
    color!: string;
    temperature: number | undefined;
    divergence: number | undefined;
}
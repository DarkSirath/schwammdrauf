export class Sensor{
    street!: string;
    altitude!: number;
    setup!: string;
    belongsTo!: string;
}
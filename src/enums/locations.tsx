import { fromLonLat } from "ol/proj";

export const CENTER = fromLonLat([10.885, 49.902])
export const ALTES_RATHAUS = fromLonLat([10.88678, 49.89163])
export const KLIMASTATION_SENDELBACH = fromLonLat([10.92051, 49.87433])
export const KLIMASTATION_LÖWENBRÜCKE = fromLonLat([10.88769, 49.89833])
export const WARTEHAEUSCHEN_STAUFFENBERG = fromLonLat([10.911634741565448, 49.898465359735916])
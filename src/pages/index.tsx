import React, { useCallback, useEffect, useState } from "react";
import Image from 'next/image'
import Head from 'next/head'
import Link from 'next/link'
import {createMap, setLayer, panTo, adjustZoneLayerValues, resetZoneLayerValues, adjustSensorLayerValues, hoverZoneFromText, hoverZone, setElementAsOverlayElement} from '@/services/createMap'
import { Feature, Map, Overlay } from "ol";
import { fetchCSVData, fetchJSONData } from '@/services/fetchData'
import * as Layers from "@/enums/layers";
import * as Locations from "@/enums/locations";
import { adjustBinTreshold, createSensorDiagram, processSensorData } from "@/services/diagrams/createSensorDiagram";
import { createZoneDiagram, processZoneData } from "@/services/diagrams/createZoneDiagram";
import { adjustScrollingPointer, getDateFromProgress } from "@/services/diagrams/diagramHelper";
import { createSunDiagram, processSunData } from "@/services/diagrams/createSunDiagram";
import ContentBox from "@/components/ContentBox";
import LegendBox from "@/components/LegendBox";
import DownArrow from "@/components/DownArrow";
import DefaultTooltip from "@/components/tooltips/DefaultTooltip";
import SendelbachTooltip from "@/components/tooltips/SendelbachTooltip";
import SensorTooltip from "@/components/tooltips/SensorTooltip";
import ZoneTooltip from "@/components/tooltips/ZoneTooltip";
import StauffenbergTooltip from "@/components/tooltips/StauffenbergTooltip";
import { Sensor } from "@/classes/tooltips/Sensor";
import { Point, Polygon } from "ol/geom";
import { getCenter } from "ol/extent";
import { Zone } from "@/classes/tooltips/Zone";

function Home({ initialData }:any) {
    const [map, setMap] = useState<Map | null>(null);
    const [layerToSelect, setLayerToSelect] = useState<string | null>(null);
    const [locationToSelect, setLocationToSelect] = useState<any | null>(null);
    const [isProcessingLayer, setIsProcessingLayer] = useState<boolean>(false);
    const [isProcessingLocation, setIsProcessingLocation] = useState<boolean>(false);
    const [isProcessingProgress, setIsProcessingProgress] = useState<boolean>(false);
    const [sensorDiagram, setSensorDiagram] = useState<any | null>(null);
    const [zoneDiagram, setZoneDiagram] = useState<any | null>(null);
    const [dailyDiagram, setDailyDiagram] = useState<any | null>(null);
    const [sunDiagram, setSunDiagram] = useState<any | null>(null);
    const [yProgress, setYProgress] = useState<number>(0);
    const [selectedSensor, setSelectedSensor] = useState<Sensor>()
    const [selectedZone, setSelectedZone] = useState<Zone>()

    const stationsData = initialData["stations"]
    const lczData = initialData["lcz_data"]
    const lczInfo = initialData["lcz_info"]
    const sensorData = processSensorData(initialData["stations"])
    const zonesData = processZoneData(initialData["zones_data"])
    const dailyData = processZoneData(initialData["daily_data"])
    const sunData = processSunData(initialData["sun_data"])

    /* 
        In this code, the useEffect hook will execute the createMap() function on the client side after the component is mounted. 
        By providing an empty dependency array ([]), the effect will only run once, ensuring the createMap() function is called 
        only on the client side and not during server-side rendering. 
    */
    
    // from https://shipshape.io/blog/wait-for-page-load-in-react/
    useEffect(() => {
        const onPageLoad = () => {
            updateDiagrams();
            createNewMap();
            window.addEventListener('scroll', () => updateProgress());
            window.addEventListener('resize', () => updateDiagrams())
        };
        // Check if the page has already loaded
        if (document.readyState === 'complete') {
            onPageLoad();
        } else {
            window.addEventListener('load', onPageLoad);
            // Remove the event listener when component unmounts
            return () => window.removeEventListener('load', onPageLoad);
        }
    }, []);

    function updateDiagrams(){
        refreshSensorDiagram();
        refreshZoneDiagram();
        refreshDailyDiagram();
        refreshSunDiagram();
    }

    function updateProgress(){
        const totalHeight = document.documentElement.scrollHeight-window.innerHeight
        const progress = window.scrollY / totalHeight
        setYProgress(progress)
    }
    
    function refreshSensorDiagram(){
        var newDiagram:any = {}
        var [svg, minX, maxX] = createSensorDiagram(sensorData)
        newDiagram["svg"] = svg
        newDiagram["minX"] = minX
        newDiagram["maxX"] = maxX
        setSensorDiagram(newDiagram);
    }

    function refreshZoneDiagram(){
        var newDiagram:any = {}
        const zone_colors = ["#006a00","#b9db79","#d10000","#ff0000","#ff6600","#ff9955","#bcbcbc","#0000ff"]
        const zone_labels = ["LCZ A","LCZ D","LCZ 2","LCZ 3","LCZ 5","LCZ 6","LCZ 8","DWD"]
        var [svg, minX, maxX] = createZoneDiagram(zonesData,"zone_diagram",9,26, false, zone_colors, zone_labels)
        newDiagram["svg"] = svg
        newDiagram["minX"] = minX
        newDiagram["maxX"] = maxX
        setZoneDiagram(newDiagram);
    }

    function refreshDailyDiagram(){
        var newDiagram:any = {}
        const zone_colors = ["#006a00","#b9db79","#d10000","#ff0000","#ff6600","#ff9955","#bcbcbc","#000000"]
        const zone_labels = ["LCZ A","LCZ D","LCZ 2","LCZ 3","LCZ 5","LCZ 6","LCZ 8","20°"]
        var [svg, minX, maxX] = createZoneDiagram(dailyData,"daily_diagram",18,40, true, zone_colors, zone_labels)
        newDiagram["svg"] = svg
        newDiagram["minX"] = minX
        newDiagram["maxX"] = maxX
        setDailyDiagram(newDiagram);
    }

    function refreshSunDiagram(){
        var newDiagram:any = {}
        var [svg, minX, maxX] = createSunDiagram(sunData,"sun_diagram",-20,60)
        newDiagram["svg"] = svg
        newDiagram["minX"] = minX
        newDiagram["maxX"] = maxX
        setSunDiagram(newDiagram);
    }

    function createNewMap(){
        const newMap = createMap(stationsData, lczData);
        var tooltip:any = document.getElementById('tooltip');
        var overlay = new Overlay({
            element: tooltip,
            offset: [0, -20],
            positioning: 'bottom-center'
        });
        newMap.addOverlay(overlay)
        newMap.on('pointermove', evt => displayTooltip(newMap,evt,overlay));
        setMap(newMap);
    }

    function displayTooltip(map:Map, evt:any, overlay:Overlay) {
        if(screen.width < 640){ return;}
        var pixel = evt.pixel;
        var feature = map.forEachFeatureAtPixel(pixel, function(feature) {
          return feature as Feature;
        });
        const tooltip:any = overlay.getElement()
        if(!tooltip) return;
        tooltip.style.display = feature ? '' : 'none';
        if (feature) {
            const geometry:any = feature.getGeometry();
    
            if(geometry instanceof Polygon){
                const extent = geometry.getExtent()
                overlay.setPosition(getCenter(extent))
            } else if(geometry instanceof Point){
                overlay.setPosition(geometry.getCoordinates());
            }
            const layer = feature.get("layerType")
            if(layer == Layers.SENDELBACH_LAYER){
                setElementAsOverlayElement(overlay,'tooltip-sendelbach')
            }else if(layer == Layers.LOEWENBRUECKE_LAYER){
                setElementAsOverlayElement(overlay,'tooltip-loewenbruecke')
            }else if(layer == Layers.SENSOR_LAYER){
                const newSensor:Sensor = {
                    street: feature.get('street'),
                    altitude: feature.get('altitude'),
                    setup: feature.get('setup'),
                    belongsTo: feature.get('belongto'),
                }
                setSelectedSensor(newSensor);
                setElementAsOverlayElement(overlay,'tooltip-sensoren')
            }else if(layer == Layers.ZONE_LAYER){
                const isSectionTemperature = feature.get("status") == "zone_temp"
                const isSectionTropicalNight = feature.get("status") == "zone_daily"

                const newTemperature = (isSectionTemperature || isSectionTropicalNight)? feature.get('value').toFixed(1): undefined
                const newDivergence = isSectionTemperature? feature.get('div_value').toFixed(1): undefined

                const current_lcz = feature.get('lcz')
                var current_description = ""
                const zones = lczInfo["zones"]
                for(let i=0;i < zones.length; i++){
                    if(zones[i]["lcz"] == current_lcz){
                        current_description = zones[i]["description"]
                    }
                }

                const newZone:Zone ={
                    name: feature.get('name'),
                    lcz: current_lcz,
                    description: current_description,
                    color: feature.get('fill'),
                    temperature: newTemperature,
                    divergence: newDivergence,
                }
                setSelectedZone(newZone);
                setElementAsOverlayElement(overlay,'tooltip-zoneDescription')
                hoverZone(map, feature)
            }else if(layer == Layers.STAUFFENBERG_LAYER){
                setElementAsOverlayElement(overlay,'tooltip-stauffenberg')
            }
        }else{
            hoverZone(map,null)
        }
    };

    const updateLayer = useCallback((layer:any) =>{
        if(map && layer && layer != layerToSelect && isProcessingLayer == false){
            setIsProcessingLayer(true);
            setLayerToSelect(layer)
            setLayer(map, layer)

            setTimeout(() => {
                setIsProcessingLayer(false);
            },200);
        }
    },[map, layerToSelect, isProcessingLayer])

    const updateLocation = useCallback((location:any) => {
        const isValidLocation = location && location.location
        const isNewLocation = locationToSelect == null || location.location != locationToSelect.location
        if(isProcessingLocation) return;
        if(!map || isValidLocation==false || isNewLocation==false) return;
        
        setIsProcessingLocation(true);
        setLocationToSelect(location)
        panTo(map, location)
        setTimeout(() => {
            setIsProcessingLocation(false);
        },1000);
    },[map, locationToSelect, isProcessingLocation])

    const locationOverview = {
        "location": Locations.CENTER,
        "zoom": 13,
        "zoom-sm": 11.5,
    }
    
    const locationSendelbach = {
        "location": Locations.KLIMASTATION_SENDELBACH,
        "zoom":16,
        "zoom-sm": 14.5,
    }

    const locationLöwenbrücke = {
        "location": Locations.KLIMASTATION_LÖWENBRÜCKE,
        "zoom":16,
        "zoom-sm": 14.5,
    }

    const locationStauffenberg = {
        "location": Locations.WARTEHAEUSCHEN_STAUFFENBERG,
        "zoom":16,
        "zoom-sm": 14.5,
    }

    useEffect(() => {
        if(isProcessingProgress) return;
        setIsProcessingProgress(true)

        const scrollDebug = document.getElementById("scroll-debug")
        if(scrollDebug){
            scrollDebug.innerHTML = ""+yProgress;
        }
        if(map) resetZoneLayerValues(map, Layers.ZONE_LAYER)

        /* Zoom to location on map */
        if(0.0 <= yProgress && yProgress < 0.07){
            updateLocation(locationOverview);
        }else if(0.07 <= yProgress && yProgress < 0.14){
            updateLocation(locationSendelbach);
        }else if(0.14 <= yProgress && yProgress < 0.875){
            updateLocation(locationOverview);
        }else if(0.875 <= yProgress && yProgress < 0.93){
            updateLocation(locationStauffenberg);
        }else if(0.93 <= yProgress && yProgress < 1.0){
            updateLocation(locationOverview);
        }
        
        /* Select layer to display */
        if(0.0 <= yProgress && yProgress < 0.07){
            updateLayer(Layers.BASE_LAYER);
        }else if(0.07 <= yProgress && yProgress < 0.14){
            updateLayer(Layers.SENDELBACH_LAYER);
        }else if(0.14 <= yProgress && yProgress < 0.32){
            updateLayer(Layers.SENSOR_LAYER); 
            scrollSensorDiagram(sensorDiagram, "sensorDiagramContainer",sensorData)
        }else if(0.32 <= yProgress && yProgress < 0.86){
            updateLayer(Layers.ZONE_LAYER);
            if(0.39 <= yProgress && yProgress < 0.57){
                setElementVisible("legendDWDDifference",true);
                setElementVisible("legendTropicalNight",false);
                setElementVisible("legendBox",true)
                scrollZoneDiagram(zoneDiagram, "zone_temp","zoneDiagramContainer", zonesData)
            }else if(0.64 <= yProgress && yProgress < 0.86){
                setElementVisible("legendDWDDifference",false);
                setElementVisible("legendTropicalNight",true);
                setElementVisible("legendBox",true)
                scrollZoneDiagram(dailyDiagram, "zone_daily","dailyDiagramContainer",dailyData)
                scrollSunDiagram(sunDiagram, "dailyDiagramContainer")
            }else{
                setElementVisible("legendDWDDifference",false);
                setElementVisible("legendTropicalNight",false);
                setElementVisible("legendBox",false)
                
            }
        }else if(0.86 <= yProgress && yProgress < 0.875){
            updateLayer(Layers.BASE_LAYER);
        }else if(0.875 <= yProgress && yProgress < 0.99){
            updateLayer(Layers.STAUFFENBERG_LAYER);
        }else if(0.93 <= yProgress && yProgress < 1.0){
            updateLayer(Layers.BASE_LAYER);
        }

        if(yProgress < 0.38 || 0.88 <= yProgress){
            setElementVisible("legendDWDDifference",false);
            setElementVisible("legendTropicalNight",false);
            setElementVisible("legendBox",false)
        }
        setTimeout(() => {
            setIsProcessingProgress(false);
        },30);
    },[yProgress, isProcessingProgress])

    function setElementVisible(name:string, visible:boolean){
        const element = document.getElementById(name)
        if(!element) return;
        element.style.display= visible?"block":"none";
    }

    function getProgress(id:string){
        const container = document.getElementById(id)
        const outerContainer = container?.parentElement
        if(!container || !outerContainer) return 0;
        let progress = (window.scrollY - outerContainer.offsetTop - container.scrollHeight) / (outerContainer.scrollHeight - container.scrollHeight)
        progress = Math.max(0, Math.min(progress, 1));
        return progress
    }

    function scrollSensorDiagram(diagram:any, id:string, data:any){
        if(!diagram) return
        const progress = getProgress(id)
        const thresholdDate = getDateFromProgress(progress, diagram["minX"], diagram["maxX"])
        adjustScrollingPointer(diagram["svg"], progress, "sensor_diagram");
        adjustBinTreshold(diagram["svg"], thresholdDate);
        if (!map) return;
        if(progress <= 0 || progress >= 1){
        
        }else{
            adjustSensorLayerValues(map, Layers.SENSOR_LAYER, thresholdDate, data)
        }
    }

    function scrollZoneDiagram(diagram:any, visName:string, id:string, data:any){
        if(!diagram) return
        const progress = getProgress(id)
        const thresholdDate = getDateFromProgress(progress, diagram["minX"], diagram["maxX"])
        adjustScrollingPointer(diagram["svg"], progress, "zone_diagram")
        if (!map) return;
        if(progress <= 0 || progress >= 1){
            
        }else{
            adjustZoneLayerValues(map, Layers.ZONE_LAYER, thresholdDate, data, visName)
        }
    }

    function scrollSunDiagram(diagram:any, id:string){
        if(!diagram) return
        const progress = getProgress(id)
        adjustScrollingPointer(diagram["svg"], progress, "sun_diagram")
    }
    
    return (
        <>
            <Head>
                <title>Schwamm Drauf - Eine Data Story</title>
            </Head>
            
            <main>
                <DefaultTooltip id="tooltip"/>
                <SendelbachTooltip id="tooltip-sendelbach"/>
                <SensorTooltip id="tooltip-sensoren" sensor={selectedSensor}/>
                <ZoneTooltip id="tooltip-zoneDescription" zone={selectedZone}/>
                <StauffenbergTooltip id="tooltip-stauffenberg"/>

                <div id="scroll-debug" className="fixed mt-5 hidden"></div>
                
                <section id="start-section" className="flex col w-full h-[100vh]">
                    <div className="absolute w-full h-[100vh]">
                        <img className="w-full h-full object-cover 
                        max-sm:hidden"
                        src="/images/SchwammDrauf_Design_Desktop.png" 
                        alt="Bild von leerem Parkplatz"
                        sizes="100vw"/>
                        <img className="w-full h-full object-cover hidden 
                        max-sm:flex"
                        src="/images/SchwammDrauf_Design_Smartphone.png" 
                        alt="Bild von leerem Parkplatz"
                        sizes="100vw"/>
                    </div>
                    <div className="flex flex-col w-[60%] h-full content-center justify-center
                    max-sm:mt-20 max-sm:justify-start max-sm:w-full">
                        <div className="flex flex-col w-full bg-white/[0.6] min-h-[100px] rounded-md backdrop-blur-sm">
                            <div className="p-8 pl-[10vw]">
                                <h1 className="relative w-full text-[120px] font-big_shoulders font-bold text-signal-color
                                max-2xl:text-8xl
                                max-xl:text-7xl
                                max-lg:text-6xl
                                max-md:text-5xl
                                max-sm:text-6xl">
                                    Schwamm Drauf!
                                </h1>
                            </div>
                        </div>
                        <div className="flex flex-col w-[80%] bg-signal-color/[0.6] mt-20 rounded-md backdrop-blur-sm
                        max-sm:mt-0">
                            <div className="p-8 pl-[10vw] flex flex-row">
                                <h2 className="text-6xl text-white font-big_shoulders w-5/6 pr-4
                                max-2xl:text-5xl
                                max-xl:text-4xl
                                max-lg:text-3xl
                                max-md:text-2xl
                                max-sm:text-3xl">
                                    Eine Data Story zur Erforschung des Bamberger Stadtklimas
                                </h2>
                                <div className="flex flex-col w-1/6 justify-center">
                                    <div className="flex flex-col bg-white w-min aspect-square border-white rounded-full">
                                        <DownArrow next_id="introduction" duration={2000}/>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <section className="relative w-full h-[3200vh] touch-pan-y
                max-lg:flex-row">
                    <div className="sticky top-0 w-[75%] flex flex-row h-[100vh] z-0 bg-white
                    max-2xl:w-[70%]
                    max-xl:w-[65%]
                    max-lg:w-full max-lg:h-[50vh] max-lg:z-10
                    max-sm:h-[30vh] max-sm:w-full max-sm:border max-sm:drop-shadow-md">
                        <div id="js-map" className="w-full h-full
                        max-sm:w-full"></div>
                        <div id="legendBox" style={{display:"none"}} className="relative z-20
                        max-sm:flex max-sm:flex-col max-sm:h-full max-sm:w-[160px]">
                            <div className="absolute translate-x-[-100%]
                            max-sm:flex max-sm:flex-col max-sm:h-full max-sm:w-full max-sm:translate-x-0">
                                <LegendBox title="Abweichung zu DWD" id="legendDWDDifference">
                                    <p>+ 3.0°C</p>
                                    <p>+ 2.5°C</p>
                                    <p>+ 2.0°C</p>
                                    <p>+ 1.5°C</p>
                                    <p>+ 1.0°C</p>
                                    <p>+ 0.5°C</p>
                                    <p>+ 0.0°C</p>
                                </LegendBox>
                                <LegendBox title="Temperatur in °C" id="legendTropicalNight">
                                    <p>38.0°C</p>
                                    <p>35.0°C</p>
                                    <p>32.0°C</p>
                                    <p>29.0°C</p>
                                    <p>26.0°C</p>
                                    <p>23.0°C</p>
                                    <p>20.0°C</p>
                                </LegendBox>
                            </div>
                        </div>
                    
                    </div>
                    <div className="absolute right-0 top-0 w-[25%] px-5 h-full bg-white drop-shadow-lg
                    max-2xl:w-[30%]
                    max-xl:w-[35%]
                    max-lg:w-full">
                        <ContentBox title="Das Stadtklima" id="introduction" next_id="DWDStation" prev_id="start-section" length={200}>
                            <p className="mb-4">
                                Die globale Erwärmung ist auch in Bamberg deutlich spürbar und verändert das Stadtklima. In den letzten Jahren 
                                zeigen sich zunehmend extreme Temperaturen; Bamberg ist eine der wärmsten Städte Bayerns. Die Trockenheit und 
                                heiße Sommertage haben negative Auswirkungen, welchen zeitnah begegnet werden muss, um auch zukünftig noch ein 
                                lebendiges Stadtleben in den Sommermontaten zu ermöglichen.
                            </p>
                            <p className="mb-4">
                                Eine wichtige Grundlage für geeignete Maßnahmen sind verlässliche Daten, welche die Besonderheiten des Bamberger 
                                Stadtklimas abbilden können. Denn das Klima kann in den einzelnen Zonen der Stadt sehr unterschiedlich sein und 
                                wird von verschiedenen Faktoren wie Bebauung, Luftzufuhr oder der Nähe zu Parks und Wäldern beeinflusst. 
                            </p>
                            <p className="mb-4">
                                Aber wie können wir eine gute Grundlage für geeignete Maßnahmen schaffen?
                                Welche Instrumente sind bereits vorhanden und wo besteht Handlungsbedarf, sowohl seitens der Politik als auch für 
                                die Bürgerinnen und Bürger der Stadt? 
                            </p>
                        </ContentBox>
                        <ContentBox title="Klimastation Bamberg" id="DWDStation" next_id="sensorDiagramContainer" prev_id="introduction" length={200}>
                            <p className="mb-4">
                                Die Klimastation Bamberg des Deutschen Wetterdienstes (DWD) befindet sich seit 2008 östlich der Kleingartenanlage 
                                Sendelbach in rein landwirtschaftlich genutztem Gebiet.
                            </p>
                            <p className="mb-4">
                                Nach internationalen Richtlinien ist der Standort nahezu ideal
                                - die dort gemessenen Werte sind aber nicht repräsentativ für das Stadtklima <Link href="#DWD"><u>[1]</u></Link>, denn die 
                                Temperaturen in der Stadt sind meist höher als im ländlichen Raum.
                            </p>
                            <p className="mb-4">
                                Die offizielle Messstation kann darüber hinaus die verschiedenen klimatischen Bedingungen in den einzelnen
                                Stadtteilen nicht erfassen. Hierfür bedarf es ein über die Sadt verteiltes Netzwerk aus Sensoren, welche 
                                das jeweilige lokale Klima messen können.
                            </p>
                            <div className="hidden max-sm:flex flex-col mt-2">
                                <div className=" aspect-[16/9] relative">
                                <Image fill
                                src="/images/Klimastation_DWD.JPG" 
                                alt="Foto der DWD Klimastation" 
                                sizes="300px"
                                style={{objectFit: "cover"}}></Image>
                                </div>
                                <p>Bild: Thomas Foken</p>
                            </div>
                        </ContentBox>
                        <ContentBox title="Sensoren im Stadtgebiet" id="sensorDiagramContainer" next_id="localeClimateZones" prev_id="DWDStation" length={600}>
                            <div className="flex flex-col h-full w-full
                                max-lg:flex-row
                                max-sm:flex-col">
                                <div className="w-full h-fit
                                max-lg:w-1/2
                                max-sm:w-full">
                                    <p className="mb-4">
                                        Im Stadtgebiet befinden sich mittlerweile <b>62 Wetter-Sensoren</b> <Link href="#NETATMO"><u>[2]</u></Link>, welche durch Bürger aufgestellt wurden und kontinuierlich Messwerte liefern. Das Inselgebiet ist besonders
                                        dicht bestückt, da dort seit Anfang 2022 zehn Sensoren des Bürgervereins Bamberg Mitte (BVM) installiert sind.                                   
                                    </p>
                                    <p className="mb-4">
                                        Im Vergleich zu den offiziellen Messstationen kann dieses engmaschige Netz von Sensoren zukünftig ein gutes Bild der klimatischen Verhältnisse in der Stadt liefern.
                                    </p>
                                    <p className="mb-4">
                                        Das Diagramm zeigt die Anzahl der jährlich in Betrieb genommenen Sensoren.
                                    </p>
                                </div>
                                <div className="flex flex-col w-full h-fit
                                max-lg:w-1/2
                                max-sm:w-full">
                                    <div id="sensor_diagram" className="flex flex-col w-full aspect-[3/2]"></div>
                                </div>
                            </div>
                        </ContentBox>
                        <ContentBox title="Lokale Klimazonen" id="localeClimateZones" next_id="zoneDiagramContainer" prev_id="sensorDiagramContainer" length={200}>
                            <p className="mb-4">
                                Das Bamberger Stadtgebiet kann anhand der Charakteristika der Bebauung in verschiedene Klimazonen eingeteilt werden, welche zur Analyse des 
                                jeweiligen lokalen Klimas dienen. Mit dieser international angewandten Kategorisierung in <em>Local Climate Zones - LCZ</em> werden Bereiche mit 
                                einheitlicher Bodendecke und Gebäudestruktur zusammengefasst <Link href="#LCZ"><u>[3]</u></Link>.
                            </p>
                            <p className="mb-4">
                                In Bamberg können folgende Zonen grob identifiziert werden:
                            </p>
                            <div className="flex flex-row mb-1">
                                <div className="bg-[#d10000] rounded-md mr-2 my-auto px-2 text-white
                                hover:bg-white hover:font-semibold hover:text-[#d10000] hover:border-2 hover:border-[#d10000]" 
                                onMouseOver={() => hoverZoneFromText(map,"2")}>LCZ 2</div>
                                <div>- kompakte, mittelhohe Bebauung</div>
                            </div>
                            <div className="flex flex-row mb-1">
                                <div className="bg-[#d10000] rounded-md mr-2 my-auto px-2 text-white
                                hover:bg-white hover:font-semibold hover:text-[#ff0000] hover:border-2 hover:border-[#ff0000]" 
                                onMouseOver={() => hoverZoneFromText(map,"3")}>LCZ 3</div>
                                <div>- kompakte, niedrige Bebauung</div>
                            </div>
                            <div className="flex flex-row mb-1">  
                                <div className="bg-[#ff6600] rounded-md mr-2 my-auto px-2 text-white
                                hover:bg-white hover:font-semibold hover:text-[#ff6600] hover:border-2 hover:border-[#ff6600]"
                                onMouseOver={() => hoverZoneFromText(map,"5")}>LCZ 5</div>
                                <div>- lockere, mittelhohe Bebauung</div>
                            </div>
                            <div className="flex flex-row mb-1">
                                <div className="bg-[#ff9955] rounded-md mr-2 my-auto px-2 text-white
                                hover:bg-white hover:font-semibold hover:text-[#ff9955] hover:border-2 hover:border-[#ff9955]"
                                onMouseOver={() => hoverZoneFromText(map,"6")}>LCZ 6</div>
                                <div>- lockere, niedrige Bebauung</div>
                            </div>
                            <div className="flex flex-row mb-1">
                                <div className="bg-[#bcbcbc] rounded-md mr-2 my-auto px-2 text-white
                                hover:bg-white hover:font-semibold hover:text-[#bcbcbc] hover:border-2 hover:border-[#bcbcbc]"
                                onMouseOver={() => hoverZoneFromText(map,"8")}>LCZ 8</div>
                                <div>- großflächige, niedrige Bebauung</div>
                            </div>
                            <div className="flex flex-row mb-1">
                                <div className="bg-[#006a00] rounded-md mr-2 my-auto px-2 text-white
                                hover:bg-white hover:font-semibold hover:text-[#006a00] hover:border-2 hover:border-[#006a00]"
                                onMouseOver={() => hoverZoneFromText(map,"A")}>LCZ A</div>
                                <div>- dichter Baumbestand</div>
                            </div>
                        </ContentBox>
                        <ContentBox title="Temperatur" id="zoneDiagramContainer" next_id="localConditions" prev_id="localeClimateZones" length={600}>
                            <div className="flex flex-col h-full w-full
                            max-lg:flex-row
                            max-sm:flex-col">
                                <div className="w-full h-fit
                                max-lg:w-1/2
                                max-sm:w-full">
                                    <p className="mb-4">
                                        Anhand der Sensordaten werden die Unterschiede zwischen den Zonen deutlich:
                                        <br/>
                                        Die höchsten Temperaturen werden in der Zone&nbsp;
                                        <span className="bg-[#ff0000] text-white rounded-md px-2
                                        hover:bg-white hover:font-semibold hover:text-[#ff0000] hover:border-2 hover:border-[#ff0000]"
                                        onMouseOver={() => hoverZoneFromText(map,"3")}>LCZ 3</span>&nbsp;
                                        gemessen, in geringem Abstand folgend dann&nbsp;
                                        <span className="bg-[#d10000] text-white rounded-md px-2
                                        hover:bg-white hover:font-semibold hover:text-[#d10000] hover:border-2 hover:border-[#d10000]"
                                        onMouseOver={() => hoverZoneFromText(map,"2")}>LCZ 2</span>,&nbsp;
                                        <span className="bg-[#ff6600] text-white rounded-md px-2
                                        hover:bg-white hover:font-semibold hover:text-[#ff6600] hover:border-2 hover:border-[#ff6600]"
                                        onMouseOver={() => hoverZoneFromText(map,"5")}>LCZ 5</span> und
                                        <span className="bg-[#bcbcbc] text-white rounded-md px-2
                                        hover:bg-white hover:font-semibold hover:text-[#bcbcbc] hover:border-2 hover:border-[#bcbcbc]"
                                        onMouseOver={() => hoverZoneFromText(map,"8")}>LCZ 8</span>,&nbsp;
                                        Von den gemessenen Zonen sind die äußeren Stadtbereiche in&nbsp;
                                        <span className="bg-[#ff9955] text-white rounded-md px-2
                                        hover:bg-white hover:font-semibold hover:text-[#ff9955] hover:border-2 hover:border-[#ff9955]"
                                        onMouseOver={() => hoverZoneFromText(map,"6")}>LCZ 6</span>&nbsp;
                                        am kältesten im Vergleich. Zu Zone&nbsp;
                                        <span className="bg-[#006a00] text-white rounded-md px-2
                                        hover:bg-white hover:font-semibold hover:text-[#006a00] hover:border-2 hover:border-[#006a00]"
                                        onMouseOver={() => hoverZoneFromText(map,"A")}>LCZ A</span>&nbsp;
                                        existieren keine Daten.
                                    </p>
                                    <p className="mb-4">
                                        Das Diagramm zeigt den wöchentlichen Durchschnittswert (Median) der Temperatur von Mai 2022 bis Oktober 2022. Auf der Karte sind zusätzlich die Abweichungen 
                                        in den einzelnen Zonen zu den Messwerten der DWD-Klimastation (Messhöhe: 5cm) dargestellt. Die Temperaturunterschiede betragen in den Sommermonaten bis zu 3°C.  
                                    </p>
                                </div>
                                <div className="flex flex-col w-full h-fit
                                max-lg:w-1/2
                                max-sm:w-full">
                                    <div id="zone_diagram" className="flex flex-col w-full aspect-[3/2]"></div>
                                </div>
                            </div>
                        </ContentBox>
                        <ContentBox title="Lokale Bedingungen" id="localConditions" next_id="dailyDiagramContainer" prev_id="zoneDiagramContainer" length={200}>
                            <p className="mb-4">
                                Bamberg steht mit einem Versiegelungsgrad von 54,24% deutschlandweit auf Platz 29 <Link href="#GDV"><u>[6]</u></Link>. Um das Stadtklima zu verbessern, dürfen Flächen in der Stadt nicht 
                                weiter versiegelt werden. Im Gegenteil ist eine <b>Entsiegelung</b> erforderlich.     
                            </p>
                            <p className="mb-4">
                                Hier zeigen sich die Effekte der lokalen Bedingungen: Enge und hohe Bebauung, wenig Grün und viel Pflaster oder Asphalt begünstigen die Wärmespeicherung.
                                Gebiete in Park- oder Flussnähe profitieren teilweise von Verdunstungs-kälte und Frischluftzufuhr. Jedoch dringen die Kaltluftschichten entlang der Flussläufe nicht weit 
                                genug in die Innenstadt vor, um dort nach heißen Sommertagen für ausreichend Abkühlung in den Nächten zu sorgen. 
                            </p>
                        </ContentBox>
                        <ContentBox title="Tropennächte" id="dailyDiagramContainer" next_id="vision" prev_id="localConditions" length={600}>
                            <div className="flex flex-col h-full w-full
                                max-lg:flex-row
                                max-sm:flex-col">
                                <div className="w-full h-fit
                                max-lg:w-1/2
                                max-sm:w-full">
                                    <p className="mb-4">
                                        Zukünftig ist damit zu rechnen, dass die Temperaturen in der Innenstadt immer häufiger Werte erreichen, welche auch gesundheits-gefährdend sein können. 
                                        Im Jahr 2022 gab es in der Inselstadt beispielsweise <b>13 Tropennächte</b> <Link href="#IRS"><u>[4]</u></Link>. Das bedeutet, dass die Temperaturen auch nachts nicht unter 20 Grad gefallen sind.
                                    </p>
                                    <p className="mb-4">
                                         Am Beispiel der <b>Tropennacht am 20. Juli 2022</b> kann man sich gut vergegenwärtigen, wie warm es bereits in der Stadt wird: 
                                        Das Diagramm zeigt den Temperaturverlauf von 24 Stunden im Vergleich zum jeweiligen Sonnenstand.
                                    </p>
                                </div>
                                <div className="flex flex-col w-full h-fit
                                max-lg:w-1/2
                                max-sm:w-full">
                                    <div id="daily_diagram" className="flex flex-col w-full aspect-[3/2]"></div>
                                    <div id="sun_diagram" className="flex flex-col w-full aspect-[3/1]"></div>
                                </div>
                            </div>
                        </ContentBox>
                        <ContentBox title="Vision Schwammstadt" id="vision" next_id="greenCity" prev_id="dailyDiagramContainer" length={200}>
                            <p className="mb-4">
                                Wie müsste die Stadt zukünftig aussehen, um der Hitze und ihren Folgen entgegenwirken zu können? Eine Antwort darauf ist die Vision 
                                der <b>"Schwammstadt"</b>: Die Stadt soll wie ein Schwamm Wasser speichern, indem Niederschlag dort gespeichert wird, wo er fällt. Hierfür 
                                wäre es u.a. erforderlich, Böden zu entsiegeln und Zisternen zu bauen. Weiterhin kann die Begrünung an Hausfassaden, Dächern und 
                                in Hinterhöfen dazu beitragen, eine Abkühlung durch Verdunstung zu schaffen. 
                            </p>
                        </ContentBox>
                        <ContentBox title="Green City Bamberg?" id="greenCity" next_id="footer" prev_id="vision" length={200}>
                            <p className="mb-4">
                                Ein erster Schritt in diese Richtung sind z.B. <b>grüne Wartehäuschen</b>: Auf sechs Quadratmeter Dachfläche werden ca. 180 Liter Wasser gespeichert, 
                                wodurch die Kanalisation bei Starkregen entlastet wird und Verdunstungskälte entstehen kann <Link href="#GRW"><u>[5]</u></Link>. Bis 2026 sollen davon dann 33 Stück in ganz Bamberg verteilt sein. Ein kleines Stück "Schwammstadt"...
                            </p>
                            <p className="mb-4">
                                Die Voraussetzung für gezielte Maßnahmen ist auch ein <b>Ausbau des Klimamessnetzes</b>, um in allen Bereichen der Stadt eine gute Abdeckung zu erreichen.
                            </p>
                            <div className="hidden max-sm:flex flex-col mt-2">
                                <div className=" aspect-[16/9] relative">
                                <Image fill
                                src="/images/Haltestelle_Stauffenberg.jpg" 
                                alt="Foto Haltestelle" 
                                sizes="300px"
                                style={{objectFit: "cover"}}></Image>
                                </div>
                                <p>Bild: Florian Aschinger</p>
                            </div>
                        </ContentBox> 
                    </div>
                </section>
            </main>
        </>
    );
}

export default Home;

export async function getStaticProps() {
    const stationsPath = "../public/data/station_data.csv"
    const lczInfoPath = "../public/knowledge/LCZ_Zones_Information.json"
    const lczMapPath = "../public/knowledge/LCZ_Zones_Map.geojson"
    const zonesPath = "../public/data/zone_data.csv"
    const dailyPath = "../public/data/daily_data.csv"
    const sunPath = "../public/data/sun_data.csv"

    const stationsData = await fetchCSVData(stationsPath);
    const lczInfo = await fetchJSONData(lczInfoPath);
    const lczData = await fetchJSONData(lczMapPath);
    const zonesData = await fetchCSVData(zonesPath);
    const dailyData = await fetchCSVData(dailyPath);
    const sunData = await fetchCSVData(sunPath);

    return {
      props: {
        initialData: {
            "stations": stationsData,
            "lcz_info": lczInfo,
            "lcz_data": lczData,
            "zones_data": zonesData,
            "daily_data": dailyData,
            "sun_data": sunData
        }
      },
    };
}

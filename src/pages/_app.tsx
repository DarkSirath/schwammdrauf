import '@/styles/globals.css'
import 'ol/ol.css'

import type { AppProps } from 'next/app'
import Head from 'next/head'

export default function App({ Component, pageProps }: AppProps) {
  return (
    <>
      <Head>
          <meta name="description" content="Schwamm Drauf - Eine Data Story" />
          <meta name="viewport" content="width=device-width, initial-scale=1" />
          <link rel="icon" href="/Logo_Wolperlab_Light.svg" />
      </Head>
      <Component {...pageProps} />
    </>
  )
}


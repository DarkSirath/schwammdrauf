import Footer from '@/components/Footer'
import { Html, Main, Head, NextScript } from 'next/document'


export default function Document() {
  return (
    <Html lang="en" className="scrollbar">
      <Head/>
      <body className="flex flex-col bg-white text-primary-text-color text-[9pt] 2xl:text-md">
        <div className="flex flex-col min-w-xs w-full h-full">
            <div className="flex flex-col h-full w-full">
                <Main />
                <NextScript/>
            </div>
        </div>
        <Footer/>
    </body>
    </Html>
  )
}
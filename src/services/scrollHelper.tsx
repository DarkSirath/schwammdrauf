export function scrollToNextElement(next_id:any, duration:number){
    const elem = document.getElementById(next_id)
    if(!elem) return;
    const start = window.scrollY;
    const target = elem.getBoundingClientRect().top + window.scrollY;
    const distance = target - start;
    // Calculate the number of frames.
    var framesPerSecond;
    if(screen.width < 640){
      framesPerSecond = 20
    }else{
      framesPerSecond = 60
    }
    const frames = Math.max(framesPerSecond * (duration / 1000), 1);

    let currentTime = 0;

    function animateScroll() {
        currentTime += 1 / frames;
        const ease = Math.sin(currentTime * (Math.PI / 2));
        const newPosition = start + distance * ease;
    
        window.scrollTo(0, newPosition);
    
        if (currentTime < 1) {
          requestAnimationFrame(animateScroll);
        }
    }
    
    animateScroll();
}
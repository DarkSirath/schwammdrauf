import * as d3 from 'd3'
import { DIAGRAM_MARGIN, addScrollingPointer, getPaddedValues, removeChildren } from './diagramHelper';

export function processSunData(data:any){
    var timeline:Date[] = [];
    var sun_data:any = []
    // sort the data according to timestamp
    data.sort(function(a:any,b:any){
        if(a == undefined || b == undefined) return;
        const timestamp_a = Number(a["timestamp"])
        const timestamp_b = Number(b["timestamp"])
        const result = timestamp_a - timestamp_b
        return result
    })
    data.forEach((element:any) => {
        const timeStamp = Number(element["timestamp"])
        const value = Number(element["sun_height"])
        const date = new Date(timeStamp * 1000)
        timeline.push(date)
        sun_data.push({time: date, value: value})
    });
    timeline = [... new Set(timeline)]

    const processedData = {
        "timeline": timeline,
        "data": sun_data
    }
    return processedData
}

export function createSunDiagram(processedData:any, elementID:string, yMin:Number, yMax:Number){
    const container = document.getElementById(elementID);
    if(!container) return [null,-1,-1];
    removeChildren(container)
    var width = container.offsetWidth - DIAGRAM_MARGIN.left - DIAGRAM_MARGIN.right;
    var height = container.offsetHeight - DIAGRAM_MARGIN.top - DIAGRAM_MARGIN.bottom;

    const timeline:Date[] = processedData["timeline"]

    
    var [paddedMinDate, paddedMaxDate] = getPaddedValues(timeline)

    const xFunc = d3.scaleTime()
        .domain([paddedMinDate, paddedMaxDate])
        .range([0, width]);

    const yFunc = d3.scaleLinear()
        .range([height, 0]);
        yFunc.domain([yMin, yMax]);

    var svg = d3.select(container)
        .append("svg")
        .attr("width", container.offsetWidth)
        .attr("height", container.offsetHeight)
        .append("g")
        .attr("transform", "translate(" + DIAGRAM_MARGIN.left + "," + DIAGRAM_MARGIN.top + ")")

    svg.append("g")
        .attr("transform", "translate(0," + height + ")")
        .call(d3.axisBottom<Date>(xFunc).ticks(d3.timeHour.every(4)).tickFormat(d3.timeFormat("%H:%M")));
    
    svg.append("g")
        .call(d3.axisLeft(yFunc).ticks(0).tickFormat(() => ""));
    svg.append("g")
        .call(d3.axisRight(yFunc).ticks(0).tickFormat(() => ""));
    
    const line:any = d3.line()
    .x(function(d:any) { return xFunc(d.time) })
    .y(function(d:any) { return yFunc(d.value) })

    const data = processedData["data"]

    // from https://d3-graph-gallery.com/graph/line_color_gradient_svg.html
    // Set the gradient
    svg.append("linearGradient")
      .attr("id", "line-gradient")
      .attr("gradientUnits", "userSpaceOnUse")
      .attr("x1", 0)
      .attr("x2", 0)
      .attr("y1", yFunc(yMin))
      .attr("y2", yFunc(yMax))
      .selectAll("stop")
        .data([
            {offset: "0%", color: "black"},
            {offset: "15%", color: "black"},
            {offset: "25%", color: "purple"},
            {offset: "35%", color: "orange"},
            {offset: "100%", color: "orange"}
        ])
      .enter().append("stop")
        .attr("offset", function(d) { return d.offset; })
        .attr("stop-color", function(d) { return d.color; });

    svg.append("path")
        .datum(data)
        .attr("fill", "none")
        .attr("stroke", "url(#line-gradient)" )
        .attr("stroke-width", 1.5)
        .attr("d", line)
    svg.append("circle")
        .attr("cx",width * 0.1)
        .attr("cy", height * 0.85 - 5)
        .attr("r", 4)
        .attr("fill", "orange")
        .attr("stroke", "#000000")
        .attr("stroke-width", 1.5)
    svg.append("text")
        .attr("x", width * 0.1 + 20)
        .attr("y", height * 0.85 )
        .text("Sonnenstand")
        .style("font-size", "10px")
        .attr("alignment-baseline","middle")

    addScrollingPointer(svg,xFunc,yFunc,paddedMinDate,yMin,yMax);
    return [svg, paddedMinDate, paddedMaxDate];
}
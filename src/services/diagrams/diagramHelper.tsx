import * as d3 from 'd3'

export const DIAGRAM_MARGIN = {top: 10, right: 10, bottom: 30, left: 40};

export function addScrollingPointer(svg:any, xFunc:any, yFunc:any, xMin:any, yMin:any, yMax:any){
    svg.append("line")
        .attr("id","pointer")
        .attr("x1", xFunc(xMin) )
        .attr("x2", xFunc(xMin) )
        .attr("y1", yFunc(yMin))
        .attr("y2", yFunc(yMax))
        .attr("stroke", "grey")
        .attr("stroke-dasharray", "4")
}

export function getPaddedValues(dateArray:Date[], padding:number=0.05){
    const minDate = d3.min(dateArray) as Date;
    const maxDate = d3.max(dateArray) as Date;
    const dateRange = maxDate.getTime() - minDate.getTime();
    const paddingValue = dateRange * padding;
   
    const paddedMinDate = new Date(minDate.getTime() - paddingValue)
    const paddedMaxDate = new Date(maxDate.getTime() + paddingValue)

    return [paddedMinDate, paddedMaxDate]
}

export function removeChildren(container:HTMLElement){
    const children = container.childNodes
    for(let i=0;i<children.length;i++){
        container.removeChild(children[i])
    }
}

export function adjustScrollingPointer(svg:any, progress:number, elementID:string){
    const container = document.getElementById(elementID);
    if(!container) return;
    var width = container.offsetWidth - DIAGRAM_MARGIN.left - DIAGRAM_MARGIN.right;
    
    svg.select("#pointer")
        .attr("x1", width * progress)
        .attr("x2", width * progress)
}


export function getDateFromProgress(progress:number, xMin:Date, xMax:Date){
    const range = xMax.getTime() - xMin.getTime()
    const tresholdDate = new Date(xMin.getTime() + range * progress)
    return tresholdDate;
}
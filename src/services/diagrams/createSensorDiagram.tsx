import * as d3 from 'd3'
import { DIAGRAM_MARGIN, addScrollingPointer, getPaddedValues, removeChildren } from './diagramHelper';
// from https://d3-graph-gallery.com/graph/histogram_basic.html
// augmented using updated functions: https://devdocs.io/d3~7/d3-array#_bin



export function toDate(setupString:string){
    const date_part:any = setupString.split("/")
    return new Date(date_part[0],date_part[1],date_part[2])
}

export function processSensorData(data:any){
    var dateArray:Date[] = [];
    data.forEach((element:any) => {
        dateArray.push(toDate(element["setup_date"]))
    });
    return dateArray
}

export function createSensorDiagram(dateArray:Date[]){
    const container = document.getElementById("sensor_diagram");
    if(!container) return [null,-1,-1];
    removeChildren(container)
    
    var width = container.offsetWidth - DIAGRAM_MARGIN.left - DIAGRAM_MARGIN.right;
    var height = container.offsetHeight - DIAGRAM_MARGIN.top - DIAGRAM_MARGIN.bottom;
    

    const amountOfBins = 24
    const amountOfXLabels = 5
    const amountOfYLabels = 7
    const paddingBetweenBins = 2 // in px

    var [paddedMinDate, paddedMaxDate] = getPaddedValues(dateArray)

    const xFunc:any = d3.scaleTime()
        .domain([paddedMinDate, paddedMaxDate])
        .range([0, width]);
    
    
    const bin = d3.bin()
        .value(d => d)
        .domain(xFunc.domain())
        .thresholds(xFunc.ticks(amountOfBins));
        
    let bins = bin(dateArray.map(date => date.getTime()));

    const yMin = 0;
    const yMax = d3.max(bins, function(d:any) { return d.length; })

    const yFunc = d3.scaleLinear()
        .range([height, 0]);
        yFunc.domain([yMin, yMax]);

    var svg = d3.select(container)
        .append("svg")
        .attr("width", container.offsetWidth)
        .attr("height", container.offsetHeight)
        .append("g")
        .attr("transform", "translate(" + DIAGRAM_MARGIN.left + "," + DIAGRAM_MARGIN.top + ")")

    svg.append("g")
        .attr("transform", "translate(0," + height + ")")
        .call(d3.axisBottom(xFunc).ticks(amountOfXLabels));

    svg.append("g")
        .call(d3.axisLeft(yFunc).ticks(amountOfYLabels));

    svg.selectAll("rect")
        .data(bins)
        .enter()
        .append("rect")
        .attr("x", 1)
        .attr("transform", function(d) { return "translate(" + xFunc(d.x0) + "," + yFunc(d.length) + ")"; })
        .attr("width", function(d) { return xFunc(d.x1) - xFunc(d.x0) - paddingBetweenBins; })
        .attr("height", function(d) { return height - yFunc(d.length); })
        .style("fill", "#69b3a2")

    addScrollingPointer(svg,xFunc,yFunc,paddedMinDate,yMin,yMax);

    return [svg, paddedMinDate, paddedMaxDate];
}

export function adjustBinTreshold(svg:any, tresholdDate:Date){
    svg.selectAll("rect")
        .style("fill", function(d:any){ if(d.x0 < tresholdDate){return "orange"} else {return "#69b3a2"}})
}
import * as d3 from 'd3'
import { DIAGRAM_MARGIN, addScrollingPointer, getPaddedValues, removeChildren } from './diagramHelper';
import { lczToZoneID } from '../createMap';
// from https://d3-graph-gallery.com/graph/histogram_basic.html
// augmented using updated functions: https://devdocs.io/d3~7/d3-array#_bin

export function processZoneData(data:any){
    var timeline:Date[] = [];
    var zones:any[] = []
    for(let i=0;i<=7;i++){
        zones.push([])
    }
    // sort the data according to timestamp
    data.sort(function(a:any,b:any){
        if(a == undefined || b == undefined) return;
        const timestamp_a = Number(a["scaled_timestamp"])
        const timestamp_b = Number(b["scaled_timestamp"])
        const result = timestamp_a - timestamp_b
        return result
    })
    data.forEach((element:any) => {
        const timeStamp = Number(element["scaled_timestamp"])
        const value = Number(element["mean_temp"])
        const div_value = Number(element["div_temp"])
        const lcz = lczToZoneID(element["lcz"])
        const date = new Date(timeStamp * 1000)
        timeline.push(date)
        zones[lcz].push({time: date, value: value, div_value: div_value})
    });
    timeline = [... new Set(timeline)]

    const processedData = {
        "timeline": timeline,
        "zones": zones
    }
    return processedData
}

export function createZoneDiagram(processedData:any, elementID:string, yMin:Number, yMax:Number, 
    isDaily:boolean, zone_colors:string[], zone_labels:string[]){
    const container = document.getElementById(elementID);
    if(!container) return [null,-1,-1];
    removeChildren(container)
    var width = container.offsetWidth - DIAGRAM_MARGIN.left - DIAGRAM_MARGIN.right;
    var height = container.offsetHeight - DIAGRAM_MARGIN.top - DIAGRAM_MARGIN.bottom;

    const amountOfLabels = 5

    const timeline:Date[] = processedData["timeline"]

    var [paddedMinDate, paddedMaxDate] = getPaddedValues(timeline)

    const xFunc = d3.scaleTime()
        .domain([paddedMinDate, paddedMaxDate])
        .range([0, width]);

    const yFunc = d3.scaleLinear()
        .domain([yMin, yMax])
        .range([height, 0]);

    var svg = d3.select(container)
        .append("svg")
        .attr("width", container.offsetWidth)
        .attr("height", container.offsetHeight)
        .append("g")
        .attr("transform", "translate(" + DIAGRAM_MARGIN.left + "," + DIAGRAM_MARGIN.top + ")")

    if(isDaily){
        svg.append("g")
            .attr("transform", "translate(0," + height + ")")
            .call(d3.axisBottom<Date>(xFunc).ticks(d3.timeHour.every(4)).tickFormat(d3.timeFormat("%H:%M")));
    }else{
        svg.append("g")
            .attr("transform", "translate(0," + height + ")")
            .call(d3.axisBottom<Date>(xFunc).ticks(d3.timeMonth.every(2)).tickFormat(d3.timeFormat("%b")));
    }

    svg.append("g")
        .call(d3.axisLeft(yFunc).ticks(amountOfLabels));
    
    const line:any = d3.line()
    .x(function(d:any) { return xFunc(d.time) })
    .y(function(d:any) { return yFunc(d.value) })

    var count = 0;
    for(let i=0;i<=7;i++){
        const zone = processedData["zones"][i]
        if(zone.length == 0) {continue;}

        const p = svg.append("path")
            .datum(zone)
            .attr("fill", "none")
            .attr("stroke", zone_colors[i])
            .attr("stroke-width", 1.5)
            .attr("d", line)
        if(isDaily && i == 7){
            p.style('stroke-dasharray', '5,3')
        }
        count++;
    }

    svg.append("rect")
            .attr("x", width * 0.1 - 5)
            .attr("y", height * 0.85 - 55)
            .attr("width", 60)
            .attr("height", 60)
            .attr("fill", "#ffffff")

    count = 0;
    for(let i=0;i<=7;i++){
        const zone = processedData["zones"][i]
        if(zone.length == 0) {continue;}
        
        svg.append("circle")
            .attr("cx",width * 0.1)
            .attr("cy", height * 0.85 - (5 + (count*10)))
            .attr("r", 4)
            .style("fill", zone_colors[i])
        svg.append("text")
            .attr("x", width * 0.1 + 20)
            .attr("y", height * 0.85 - (count*10))
            .text(zone_labels[i])
            .style("font-size", "10px")
            .attr("alignment-baseline","middle")
        count++;
    }

    addScrollingPointer(svg,xFunc,yFunc,paddedMinDate,yMin,yMax);
    return [svg, paddedMinDate, paddedMaxDate];
}
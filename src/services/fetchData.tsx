import fs from 'fs'
import csv from 'csv-parser'
import path from 'path'

export async function fetchCSVData(fileName:any){
    var filePath = path.join(process.cwd(), 'public', fileName)
    return new Promise((resolve, reject) => {
        const results:any = [];

        fs.createReadStream(filePath)
        .pipe(csv({ separator: ';' }))
        .on('data', (data) => results.push(data))
        .on('end', () => resolve(results))
        .on('error', (error) => reject(error));
    });
};

export async function fetchJSONData(fileName:any){
    var filePath = path.join(process.cwd(), 'public', fileName)
    return new Promise((resolve, reject) => {
        fs.readFile(filePath, 'utf8', (err, data) => {
            if (err) {
              reject(err);
              return;
            }
            try {
              const jsonData = JSON.parse(data);
              resolve(jsonData);
            } catch (error) {
              reject(error);
            }
        });
    });
};


import { Feature, Map, Overlay, View } from 'ol';
import * as DrawHelper from '@/services/drawHelper'
import * as Projections from '@/enums/projections'
import * as Locations from '@/enums/locations'
import * as Layers from '../enums/layers';
import VectorSource from 'ol/source/Vector';
import { Zoom } from 'ol/control';
import { easeIn } from 'ol/easing.js';
import { toDate } from './diagrams/createSensorDiagram';


export function createMap(stationsData:any, lczData:any){
    // delete old viewport first (if one exists)
    const old_viewport = document.querySelectorAll(".ol-viewport");
    old_viewport.forEach(elem =>{
        elem.remove()
    })

    const map = new Map({
        view: new View({
            projection: Projections.WEB_MERCATOR,
            center: Locations.CENTER,
            zoom: 14,
        }),
        target: "js-map",
        interactions: []
    })
    map.getControls().forEach(function(control) {
        if (control instanceof Zoom) {
          map.removeControl(control);
        }
    });

    map.getInteractions().forEach(function(interaction) {
        console.log(interaction.constructor.name)
    });
    
    const baseLayer = DrawHelper.createBaseMap(map);
    baseLayer.set("name",Layers.BASE_LAYER)
    baseLayer.setVisible(true)
    const zoneLayer = DrawHelper.drawZones(map, lczData);
    zoneLayer.set("name",Layers.ZONE_LAYER)
    zoneLayer.setVisible(false)
    const sensorLayer = DrawHelper.drawPointsFromData(map, stationsData, Layers.SENSOR_LAYER);
    sensorLayer.set("name",Layers.SENSOR_LAYER)
    sensorLayer.setVisible(false)
    const stationSendelbachLayer = DrawHelper.drawCoordinate(map, Locations.KLIMASTATION_SENDELBACH,Layers.SENDELBACH_LAYER);
    stationSendelbachLayer.set("name",Layers.SENDELBACH_LAYER)
    stationSendelbachLayer.setVisible(false)
    const stationLoewenbrueckeLayer = DrawHelper.drawCoordinate(map, Locations.KLIMASTATION_LÖWENBRÜCKE,Layers.LOEWENBRUECKE_LAYER);
    stationLoewenbrueckeLayer.set("name",Layers.LOEWENBRUECKE_LAYER)
    stationLoewenbrueckeLayer.setVisible(false)
    const haltestelleStauffenbergLayer = DrawHelper.drawCoordinate(map, Locations.WARTEHAEUSCHEN_STAUFFENBERG,Layers.STAUFFENBERG_LAYER);
    haltestelleStauffenbergLayer.set("name",Layers.STAUFFENBERG_LAYER)
    haltestelleStauffenbergLayer.setVisible(false)
    
    return map;
}

export function setElementAsOverlayElement(overlay:Overlay, elementID:string){
    const newTooltip:any = document.getElementById(elementID)?.cloneNode(true);
    newTooltip.style.display = 'block'
    overlay.setElement(newTooltip)
}

export function setLayer(map:Map, layer:string){
    const layers = map.getAllLayers().filter(layer => layer.get("name") !== Layers.BASE_LAYER)
            
    for(let i=0;i < layers.length; i++){
        const layerName = layers[i].get("name")
        if(layerName === layer){
            layers[i].setVisible(true)
        }else{
            layers[i].setVisible(false)
        }
    }
}

function getLayer(map:Map, layerName:string){
    const layer = map.getAllLayers().filter(layer => layer.get("name") == layerName)[0];
    return layer;
}

export function lczToZoneID(lcz:string){
    switch(lcz){
        case "A": return 0;
        case "D": return 1;
        case "2": return 2;
        case "3": return 3;
        case "5": return 4;
        case "6": return 5;
        case "8": return 6;
        case "DWD": return 7;
    }
    throw new Error("Conversion Error")
}

export function hoverZone(map:Map, feature:Feature|null){
    const layer = getLayer(map,Layers.ZONE_LAYER)
    const source = layer.getSource() as VectorSource
    const features = source.getFeatures()
    for(let i=0;i<features.length;i++){
        const props = features[i]["values_"]
        if(feature == features[i]){
            props["hovered"] = true
        }else{
            props["hovered"] = false
        }
    }
    layer.getSource()?.changed()
}

export function hoverZoneFromText(map:Map|null, lcz:string){
    if(!map) return;
    const layer = getLayer(map,Layers.ZONE_LAYER)
    const source = layer.getSource() as VectorSource
    const features = source.getFeatures()
    for(let i=0;i<features.length;i++){
        const props = features[i]["values_"]
        if(props["lcz"] == lcz){
            props["hovered"] = true
        }else{
            props["hovered"] = false
        }
    }
    layer.getSource()?.changed()
}


export function adjustSensorLayerValues(map:Map, layerName:string, thresholdDate:Date, data:any){
    const layer = getLayer(map, layerName)
    const source = layer.getSource() as VectorSource
    const features = source.getFeatures()

    for(let i=0;i<features.length;i++){
        const props = features[i]["values_"]
        const setup = toDate(props["setup"])
        if(setup <= thresholdDate){
            props["visible"] = true
        }else{
            props["visible"] = false
        }
    }
    source.changed()
}


export function resetZoneLayerValues(map:Map, layerName:string){
    const layer = getLayer(map, layerName)
    const source = layer.getSource() as VectorSource
    const features = source.getFeatures()
    for(let i=0;i<features.length;i++){
        const props = features[i]["values_"]
        props["status"] = "overview"
    }
    source.changed()
}

export function adjustZoneLayerValues(map:Map, layerName:string, thresholdDate:Date, data:any, visName:string){
    const layer = getLayer(map, layerName)
    const source = layer.getSource() as VectorSource
    const features = source.getFeatures()
    for(let i=0;i<features.length;i++){
        const props = features[i]["values_"]
        const zoneID = lczToZoneID(String(props["lcz"]))
        if(zoneID == undefined) continue;
        var zoneData = data["zones"][zoneID]
        var zoneData = data["zones"][zoneID]

        // no data available
        if(zoneData.length == 0){
            props["status"] = "no_data"
            continue;
        }else{
            props["status"] = visName
        }
        for(let k=0;k<zoneData.length;k++){
            const curr = zoneData[k]
            if(curr && thresholdDate == curr.time){
                props["div_value"] = curr.div_value
                props["value"] = curr.value
                break;
            }
            if(k >= zoneData.length || zoneData[k+1] == undefined){
                props["status"] = "no_data"
                break;
            }
            const next = zoneData[k+1]
            if(thresholdDate > curr.time && thresholdDate < next.time){
                const range = next.time.getTime() - curr.time.getTime()
                const diff1 = (thresholdDate.getTime() - curr.time.getTime())/range
                const diff2 = 1 - diff1
                props["div_value"] = curr.div_value * diff2 + next.div_value * diff1
                props["value"] = curr.value * diff2 + next.value * diff1
                props["div_value"] = curr.div_value * diff2 + next.div_value * diff1
                props["value"] = curr.value * diff2 + next.value * diff1
                break;
            }
        }
    }
    source.changed()
}

function getZoomLevelFromScreenWidth(location:any){
    if(screen.width <= 640){
        return location["zoom-sm"];
    }else{
        return location["zoom"]
    }
}

export function panTo(map:Map, location:any){
    map.getView().animate({
        center: location["location"],
        zoom: getZoomLevelFromScreenWidth(location),
        duration: 2000,
        easing: easeIn,
    })
    console.log("-- pan to "+location["location"] + " with zoom "+location["zoom"])
}

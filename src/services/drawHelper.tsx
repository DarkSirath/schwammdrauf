import { Map, Feature } from 'ol';
import { Fill, Stroke, Circle, Style } from 'ol/style';
import { Vector as VectorLayer, Tile } from 'ol/layer';
import { OSM, Vector } from 'ol/source'
import { Point, Polygon } from 'ol/geom'
import { Delaunay } from 'd3-delaunay'
import * as Projections from '@/enums/projections'
import { Coordinate } from 'ol/coordinate';
import { asArray } from 'ol/color';
import { ZONE_LAYER } from '../enums/layers';

// from: https://www.pocketmagic.net/voronoi-diagrams-openlayers-3/
// three color gradient, starting from color1(0%) to color2(50%) and color3(100%)
function gradient(color1:any, color2:any, color3:any, alpha:any, percent:any) {
    if (percent > 100) percent = 100;
    function makeChannel(a:any, b:any, c:any) { 
		if (percent < 50)
			return(a + Math.round((b-a)*(percent/50))); 
		else 
			return(b + Math.round((c-b)*((percent-50)/50))); 
    }
    var r = makeChannel(color1.r, color2.r, color3.r);
    var g = makeChannel(color1.g, color2.g, color3.g);
    var b = makeChannel(color1.b, color2.b, color3.b);

    return "rgba(" +r + ", " + g + ", " + b + ", " + alpha + ")";
}

export function createBaseMap(map:Map){
    const baseMap = new Tile({
        source: new OSM({
            url: "https://api.maptiler.com/maps/basic-v2/{z}/{x}/{y}.png?key=tagh7jgT322vK988KC5c",
            attributions: '<a href="https://www.maptiler.com/copyright/" target="_blank">&copy; MapTiler</a> <a href="https://www.openstreetmap.org/copyright" target="_blank">&copy; OpenStreetMap contributors</a>'
        }),
        visible: true,
    })
    map.addLayer(baseMap);
	return baseMap;
}

export function drawVoronoiFromData(map:Map, data:any) {
	var data_array:any = [];
	var pos_array: any = [];

	data.forEach((element: any) => {
		data_array.push({
			coords: [element.lon, element.lat],
			value: Math.random() * 30
		})
		pos_array.push([element.lon, element.lat])
	});
	
	var delaunay = Delaunay.from(pos_array);
	
	var polygonSource = new Vector({});

	var bounds:any = [10.762734232790763,49.84517799836729,11.019262690527171,49.96411929813672] // minx, miny, maxx, maxy

	var i = 0;
	for(var triangle of delaunay.voronoi(bounds).cellPolygons()){
		try {
			
			var feature = new Feature({
				geometry: new Polygon([triangle]).transform(Projections.WGS84,Projections.WEB_MERCATOR),
				index: i,
				value: data_array[i].value
			});
				
			polygonSource.addFeature(feature);
		} catch (err) { 
			console.log("feature error:" + err + " " + i);
		}
		i++;
	}

	var polyStyleFunc = function (polygon:any) {
		var value = polygon.get("value");
		var red = { r:255, g:0, b:0 };
		var yellow = { r:255, g:255, b:0 };
		var green = { r:0, g:255, b:0 };
		
	    var color = gradient(green, yellow, red, 0.5, value * 10/3);
		return [
			new Style({
		        fill: new Fill({ color: color }),
		        stroke: new Stroke({ color: [0,0,0,0.1], width: 1 })
		    })
		];
	};

	var vectorLayer = new VectorLayer({
    	source: polygonSource,
    	style: polyStyleFunc
	});
	map.addLayer(vectorLayer);
	return vectorLayer;
}

export function drawPointsFromData(map:Map, data:any, layer:string){
	var vectorSource = new Vector({});
	

	data.forEach((element: any) => {
		var feature = new Feature({
			geometry: new Point([element.lon, element.lat]).transform(Projections.WGS84,Projections.WEB_MERCATOR),
			layerType: layer,
			device: element["device_id"],
			street: element["street"],
			altitude: Number(element["altitude"]),
			setup: element["setup_date"],
			belongto: (element["is_bvm"] == "yes")?"BVM":"Privat"
		});
		vectorSource.addFeature(feature);
	});

	var vectorLayer = new VectorLayer({
    	source: vectorSource,
		style: styleSensor
	});
	map.addLayer(vectorLayer);
	return vectorLayer;
}

export function drawCoordinate(map:Map, data:Coordinate, layer:any){
	var vectorSource = new Vector({});
	
	var feature = new Feature({
		geometry: new Point(data),
		layerType: layer,
	});
	vectorSource.addFeature(feature);
	
	var vectorLayer = new VectorLayer({
    	source: vectorSource,
		style: new Style({
				image: new Circle({
				radius: 8,
				fill: new Fill({color: 'blue'}),
				stroke: new Stroke({color: 'grey', width: 2})
			})
		})
	});
	map.addLayer(vectorLayer);
	return vectorLayer;
}

export function drawZones(map:Map, data:any){
	var vectorSource = new Vector({
		features: []
	});
	
	for(let i=0;i< data["features"].length;i++){
		const featureData = data["features"][i]
		const propertyData = featureData["properties"]
		const geometryData = featureData["geometry"]
		const coordinates = geometryData["coordinates"][0]
		
		var feature = new Feature({
			geometry: new Polygon([coordinates]).transform(Projections.WGS84,Projections.WEB_MERCATOR),
			value: -999,
			name: propertyData["name"],
			lcz: propertyData["lcz"],
			fill: propertyData["fill"],
			description: propertyData["description"],
			hovered: false,
			status: "overview",
			layerType: ZONE_LAYER,
		});
		vectorSource.addFeature(feature);
	}

	var layer = new VectorLayer({
		source: vectorSource,
		style: styleZone
	});

	map.addLayer(layer);
	return layer;
}

function styleSensor(feature:any){
	const visible = Boolean(feature.get("visible"))
	if(visible == false) return [new Style()]
	return [
		new Style({
			image: new Circle({
				radius: 8,
				fill: new Fill({color: 'blue'}),
				stroke: new Stroke({color: 'grey', width: 2})
			})
		})
	];
}

function styleZone(feature:any) {
	const status = String(feature.get("status"))
	const value = Number(feature.get("value"))
	const hovered = Boolean(feature.get("hovered"))
	var fillColor:any = []
	var strokeColor = [100,100,100,0.5]
	var strokeWidth = 1
	var zIndex = 1
	if(status == "overview"){
		if(hovered){
			fillColor = asArray(feature.get("fill"));
			fillColor[3] = 0.8 // opacity
			strokeWidth = 2
			strokeColor = [0,0,0,1]
			zIndex = 10
		}else{
			fillColor = [255,255,255,.8]
		}
	} else if(status == "zone_temp"){
		const value = Number(feature.get("div_value"))
		const red = {r: 255,g:0,b:0}
		const blue = {r: 0,g:0,b:255}
		const yellow = {r: 255,g:255,b:0}
		fillColor = gradient(blue,yellow,red,0.8,value * (100/3))
		if(hovered){
			strokeWidth = 2
			strokeColor = [0,0,0,1]
			zIndex = 10
		}
	} else if(status == "zone_daily"){
		const value = Number(feature.get("value"))
		const red = {r: 255,g:0,b:0}
		const blue = {r: 0,g:0,b:255}
		const yellow = {r: 255,g:255,b:0}
		fillColor = gradient(blue,yellow,red,0.8,(value-20) * (100/18))
		if(hovered){
			strokeWidth = 2
			strokeColor = [0,0,0,1]
			zIndex = 10
		}
	} 
	else if(status == "no_data"){
		fillColor = [255,255,255,.8]
		if(hovered){
			strokeWidth = 2
			strokeColor = [0,0,0,1]
			zIndex = 10
		}
	}
	return [
		new Style({
			fill: new Fill({ color: fillColor }),
			zIndex: zIndex,
			stroke: (strokeWidth == 0)? undefined : new Stroke({ color: strokeColor, width: strokeWidth})
		})
	];
};

